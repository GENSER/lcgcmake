#- Herwig3 ----------------------
set(thepeg_v_home <thepeg-<herwig3_<VERSION>_thepeg>_home>)
LCGPackage_Add(
    herwig3
    URL ${gen_url}/Herwig-<NATIVE_VERSION>.tar.bz2
    CONFIGURE_COMMAND ./configure --prefix=<INSTALL_DIR>
                                  --with-gsl=${GSL_home}
                                  --with-thepeg=${thepeg_v_home}
                                  --with-thepeg-headers=${thepeg_v_home}/include
                                  --with-fastjet=${fastjet_home}
    BUILD_COMMAND ${MAKE} all ${library_path}=${thepeg_v_home}/lib/ThePEG:${GSL_home}/lib:${Boost_home}/lib:${fastjet_home}/lib:$ENV{${library_path}} LIBRARY_PATH=${fastjet_home}/lib
          COMMAND ${CMAKE_COMMAND} -E chdir <SOURCE_DIR>/Contrib make ${library_path}=${thepeg_v_home}/lib/ThePEG:${GSL_home}/lib:${Boost_home}/lib:${fastjet_home}/lib:$ENV{${library_path}} CPPFLAGS=-I${fastjet_home}/include LIBRARY_PATH=${fastjet_home}/lib
 #         COMMAND ${CMAKE_COMMAND} -E chdir <SOURCE_DIR>/Contrib/AlpGen 
 #                                  make BasicLesHouchesFileReader.so HERWIGINCLUDE=-I<SOURCE_DIR>/include 
 #                                                                    ${library_path}=${thepeg_v_home}/lib/ThePEG:${GSL_home}/lib:${Boost_home}/lib:${herwig3_home}/lib/Herwig++:${fastjet_home}/lib:$ENV{${library_path}} CPPFLAGS=-I${fastjet_home}/include LIBRARY_PATH=${fastjet_home}/lib
          COMMAND ${CMAKE_COMMAND} -E chdir <SOURCE_DIR>/Contrib/AlpGen 
                                   make AlpGenHandler.so HERWIGINCLUDE=-I<SOURCE_DIR>/include
                                   ${library_path}=${thepeg_v_home}/lib/ThePEG:${GSL_home}/lib:${Boost_home}/lib:${herwig3_home}/lib/Herwig++:${fastjet_home}/lib:$ENV{${library_path}} CPPFLAGS=-I${fastjet_home}/include LIBRARY_PATH=${fastjet_home}/lib
    INSTALL_COMMAND ${MAKE} install ${library_path}=${thepeg_v_home}/lib/ThePEG:${GSL_home}/lib:${Boost_home}/lib:${HepMC_home}/lib:${fastjet_home}/lib:$ENV{${library_path}} LIBRARY_PATH=${fastjet_home}/lib:${thepeg_v_home}/lib/ThePEG
            COMMAND  ${CMAKE_COMMAND} -E copy <SOURCE_DIR>/Contrib/AlpGen/AlpGenHandler.so <INSTALL_DIR>/lib/Herwig++/      
            COMMAND  ${CMAKE_COMMAND} -E copy <SOURCE_DIR>/Contrib/AlpGen/BasicLesHouchesFileReader.so <INSTALL_DIR>/lib/Herwig++/ 
    BUILD_IN_SOURCE 1
    DEPENDS Python GSL thepeg-<herwig3_<VERSION>_thepeg> fastjet # vbfnlo njet gosam openloops madgraph
  )


